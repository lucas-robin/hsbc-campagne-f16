Cypress.Commands.add("choiceSelect", (data) => {
    cy.visit(`/${data.lang}/slide/${data.currentStep}`)
    cy.get(`[data-cy='choice-${data.choice}']`).click().should('have.class', 'active-choice')
    cy.get("[data-cy='funfact']").should('be.visible')
    cy.get("[data-cy='next']").should('be.visible').click()
    cy.url().should('include', `/${data.lang}/${data.nextStep}`)
})
