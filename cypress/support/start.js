Cypress.Commands.add("start", () => {

    cy.visit('/')
    cy.get("[data-cy='flag-fr']").click()
    cy.wait(1000)
    cy.get("[data-cy='cookie-accept']").click().should(() => {
        expect(localStorage.getItem('cookie-enabled')).to.eq('true')
    })
    cy.get("[data-cy='start']").click()

})
