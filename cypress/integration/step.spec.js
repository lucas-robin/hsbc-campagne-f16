describe('Step', function() {

    const vw = 375
    const vh = 667

    beforeEach(() => {
        cy.viewport(vw, vh)
        cy.start()
    })

    it('Complete quizz', () => {

        cy.fixture('quizz-complete').then((json) => {
            json.section.first.forEach(data => {
                cy.choiceSelect({lang: json.section.lang, currentStep: data.currentStep, choice: data.choice, nextStep: data.nextStep})
            });

            cy.get("[data-cy='continue']").click()
            cy.url().should('include', `/en/slide/6`)

            json.section.second.forEach(data => {
                cy.choiceSelect({lang: json.section.lang, currentStep: data.currentStep, choice: data.choice, nextStep: data.nextStep})
            });

            cy.get("[data-cy='continue']").click()
            cy.url().should('include', `/en/slide/11`)

            json.section.third.forEach(data => {
                cy.choiceSelect({lang: json.section.lang, currentStep: data.currentStep, choice: data.choice, nextStep: data.nextStep})
            });

            cy.url().should('include', `/en/result`)

        })

    })

})
