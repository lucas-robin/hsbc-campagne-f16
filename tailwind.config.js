/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
    theme: {
        extend: {
            colors: {
                'hsbc-blue': '#003466',
                'hsbc-red': '#DC0012',
                'hsbc-green': '#008580',
                'hsbc-submit-button': '#3E505D',
                'hsbc-submit-button-muted': '#6D8EA6',
                'hsbc-grey': '#F2F2F2',
                'hsbc-lightgrey' : '#D8D8D8',
                'hsbc-muted' : '#F5F5F5'
            },
            minHeight: {
                'cookie': '17.5rem',
                'main-home-desktop': '800px'
            },
            width: {
                'hsbc-rectangle': '40px'
            },
            fontSize: {
                'hsbc-button': '1.0625rem',
                'hsbc-lg': '1.1875rem'
            },
            height: {
                '128': '31rem'
            }
        }
    },
    variants: {},
    plugins: [
        require('tailwindcss-animatecss')({
            settings: {
                animatedSpeed: 500,
                heartBeatSpeed: 1000,
                hingeSpeed: 2000,
                bounceInSpeed: 750,
                bounceOutSpeed: 750,
                animationDelaySpeed: 1000
            },
            variants: ['responsive'],
        }),
    ]
}
