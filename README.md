# hsbc-campagne-f16

> Quiz HSBC trigger when a QRCode has been scanned

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

## Description

F-16 – Expérience digitale internationale


Après avoir scanné un QR code dans un aéroport français, les prospects sont invités à répondre à un quizz en 15 questions afin d’obtenir un Guide « Bienvenue en France » personnalisé au format PDF.

Il y a 8 versions du guide PDF qui seront envoyées par email après recueil de l’adresse via un formulaire :
Travailleur indépendant avec enfants
Travailleur indépendant sans enfant
Salarié avec enfants
Salarié sans enfant
Retraité avec enfants
Retraité sans enfant
En recherche d’emploi avec enfants
En recherche d’emploi sans enfant

L’utilisateur a la possibilité d’arrêter le quiz pour obtenir son guide au bout de 5 questions, 10 questions et 15 questions.

La 1ère question sert à sélectionner les prospects qui sont éligibles à l’expérience F16.
Les questions 2 et 3 servent à déterminer le profil correspondant au guide personnalisé.
Les 12 questions suivantes permettent de cumuler des points qui déterminent un niveau de connaissance de la France : Debutant, Amateur ou Connoisseur (en anglais dans le texte).

