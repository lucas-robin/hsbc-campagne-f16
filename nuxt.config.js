import dataFR from './assets/json/fr.json'
import dataEN from './assets/json/en.json'
import webpack from 'webpack'
require('dotenv').config()

export default {
    mode: 'spa',
    router: {
        base: process.env.NUXT_ENV_BASE_PATH
    },
    generate: {
        fallback: true
        // fallback: '404.html'
    },
    /*
     ** Headers of the page
     */
    head: {
        title: process.env.npm_package_name || '',
        meta: [
            {
                charset: 'utf-8'
            },
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1'
            },
            {
                hid: 'description',
                name: 'description',
                content: process.env.npm_package_description || ''
            }
        ],
        script: [
            {
                src: '//tags.tiqcdn.com/utag/hsbc/fr-rbwm/prod/utag.sync.js'
            }
        ],
        link: [{
            rel: 'icon',
            type: 'image/x-icon',
            href: '/favicon.ico'
        }]
    },
    /*
     ** Customize the progress-bar color
     */
    loading: {
        color: '#fff'
    },
    /*
     ** Global CSS
     */
    css: [
    ],
    /*
     ** Plugins to load before mounting the App
     */
    plugins: [
        '~/plugins/vuelidate',
        '~/plugins/utag',
        '~/plugins/config',
        '~/plugins/helpers',
        '~/plugins/cookie'
    ],
    /*
     ** Nuxt.js dev-modules
     */
    buildModules: [
        // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
        '@nuxtjs/tailwindcss',
        [
            'nuxt-i18n',
            {
                strategy: 'prefix_and_default',
                locales: ['fr', 'en'],
                defaultLocale: 'en',
                vueI18n: {
                    fallbackLocale: 'en',
                    messages: {
                        fr: dataFR,
                        en: dataEN
                    }
                }
            }
        ]
    ],
    /*
     ** Nuxt.js modules
     */
    modules: [
        // Doc: https://github.com/nuxt-community/dotenv-module
        '@nuxtjs/dotenv',
        '@nuxtjs/axios'
    ],

    /**
     * Axios config
     */
    axios: {
        // proxyHeaders: false
    },

    /**
     * Env variables
     */
    env: {
        // TEST: process.env.TEST
    },

    /*
     ** Build configuration
     */
    build: {
        /*
         ** You can extend webpack config here
         */
        extend(config, ctx) {}

    }
}
