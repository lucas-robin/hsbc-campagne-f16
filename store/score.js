export const state = () => ({
    total: 0,
    history: []
})

export const mutations = {
    incrementScore(state, count) {
        state.history.push(count)
        state.total += count
    },
    decrementScore(state) {
        state.total -= state.history[state.history.length - 1]
        state.history.pop()
    }
}
