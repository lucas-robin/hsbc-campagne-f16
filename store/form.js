
export const state = () => ({
    accessToken: null,
    formData: Object,
    pdf: Object,
    conditionalGuide: {
        freelancer: null,
        employed: null,
        retired: null,
        looking_job: null
    }
})

export const mutations = {
    updateFormData: (state, formData) => {
        // Sauvegarde le formulaire (sans PDF)
        state.formData.isSubmit = formData.isSubmit
        state.formData.firstname = formData.firstname
        state.formData.mail = formData.mail
        state.formData.gender = formData.gender
        state.formData.lastname = formData.lastname
        state.formData.wantsGuide = formData.wantsGuide
        state.formData.wantsEmail = formData.wantsEmail
        state.formData.lang = formData.lang

    },
    updateMail: (state, mail) => {
        state.formData.mail = mail
    },
    setJSONPdf: (state, obj) => {
        state.pdf = obj
    },
    storeChoiceOne: (state, choice) => {
        state.conditionalGuide[choice] = {};
    },
    storeChoiceTwo: (state, hasChildren) => {

        // On récupère le choix de Q2
        let typeStatusJob = Object.keys(state.conditionalGuide).find(key => state.conditionalGuide[key] !== null);
        state.conditionalGuide[typeStatusJob].children = hasChildren

        // Ajout le PDF aux data du formulaire
        state.formData = {
            pdf : state.conditionalGuide[typeStatusJob].children ?
                state.pdf[typeStatusJob].has_children :
                state.pdf[typeStatusJob].has_not_children
        }

    },
    SET_TOKEN: (state, token) => {
        state.accessToken = token
    }
}

export const actions = {

    /**
     * Get token oAuth2
     * @param {*} context
     */
    getToken : async function ({ commit, state, dispatch }) {

        // URL for get a token
        const url = 'https://api.ccmp.eu/services2/authorization/oAuth2/Token'

        // Body of POST request
        const params = new URLSearchParams()
        params.append('username', process.env.NUXT_ENV_CONSUMER_KEY)
        params.append('password', process.env.NUXT_ENV_CONSUMER_SECRET)
        params.append('client_id', process.env.NUXT_ENV_CLIENT_ID)
        params.append('grant_type', process.env.NUXT_ENV_GRANT_TYPE)

        // Header request
        const config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }

        // Axios request with error handler
        try {
            const { data : { access_token } } = await this.$axios.post(url, params, config)
            commit('SET_TOKEN', access_token)
        } catch({response : { data, status, headers }, request, message }) {
            if(error.response) {
                if(data) console.error(data)
                if(status) console.error(status)
                if(headers) console.error(headers)
            } else if (request) {
                console.error(request)
            } else {
                console.error("ERROR:", message)
            }
            console.error(error)
        }
    },

    /**
     * Send Data to Cheetah campagne
     * @param {*} context
     */
    submitForm: async function({ dispatch, state }) {

        // Guide by default
        let guidByDefault = this.$i18n.messages[this.$i18n.locale].pdf.employed.has_not_children;

        // PDF link attributed by language and choices selected in question 1 & 2.
        let PDF_link = state.formData.pdf ?
            `${window.location.origin}${process.env.NUXT_ENV_BASE_PATH}/pdf/${state.formData.pdf}` :
            `${window.location.origin}${process.env.NUXT_ENV_BASE_PATH}/pdf/${guidByDefault}`;

        // Campaign ID attributed by language
        const CAMPAIGN_ID_FR = process.env.NUXT_ENV_CAMPAIGN_ID_FR
        const CAMPAIGN_ID_EN = process.env.NUXT_ENV_CAMPAIGN_ID_EN
        let _campaignId = this.$i18n.locale === 'en' ? CAMPAIGN_ID_EN : CAMPAIGN_ID_FR

        // Trigger getToken action
        await dispatch('getToken')

        // URL for trigger the campaign
        const url = 'https://api.ccmp.eu/services2/api/Campaign/Trigger'
        const { formData : { mail, lang, pdf, gender, firstname, lastname, wantsEmail }, accessToken } = state

        // Body of POST request
        let body = JSON.stringify({
            _campaignId,
            _responsePayload: false,
            _data: {
                Airport_data_collecting:[{
                    email: mail,
                    language: lang ? lang : this.$i18n.locale.toUpperCase(),
                    PDF_link,
                    api_call_timestamp: new Date().toISOString(),
                    global_email_optin: wantsEmail ? "yes" : "no"
                }]
            },
            _campaignMetadata: {
                civility: gender,
                firstname: firstname,
                last_name: lastname,
            }
        })

        // Header request
        const config = {
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        }

        // Axios request with error handler
        try {
            const res = await this.$axios.post(url, body, config)
        } catch({response : { data, status, headers }, request, message }) {
            if(error.response) {
                if(data) console.error(data)
                if(status) console.error(status)
                if(headers) console.error(headers)
            } else if (request) {
                console.error(request)
            } else {
                console.error("ERROR:", message)
            }
            console.error(error)
        }
    }

}
