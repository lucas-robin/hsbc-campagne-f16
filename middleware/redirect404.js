/**
 * @deprecated
 * TODO: Refactor this middleware
 */
export default function ({ params, app }) {

    let { router : { app : { _route : { path } } } } = app

    if (path.includes('slide'))
        return slideExist(params, app.i18n) ? true : $nuxt.$router.push({ path: '/' })
    else if (path.includes('redirect'))
        return redirectExist(params, app.i18n) ? true : $nuxt.$router.push({ path: '/' })
    else if (path.includes('result'))
        return resultExist(params, app.i18n) ? true : $nuxt.$router.push({ path: '/' })
}

/**
 * Retourne true|false en fonction de l'existance de la question du quizz
 * @param Number step - Numéro de la step courante (disponible dans le paramètre de l'URL)
 * @param Object json - Data i18n disponible dans assets/json/*.json
 */
function slideExist({ step }, { messages: json }) {
    let { en: { screen: { steps } } } = json
    return step >= steps.length || step <= 0 ? false : true
}

/**
 * Retourne true|false en fonction de l'existance d'un écran de sortie Q1
 * @param Number redirect - Numéro de l'ID courant (disponible dans le paramètre de l'URL)
 * @param Object json - Data i18n disponible dans assets/json/*.json
 */
function redirectExist({ id }, { messages: json }) {
    let { en: { screen: { redirects } } } = json
    return id >= redirects.length || id <= 0 ? false : true
}

/**
 * Retourne true|false en fonction de l'existance d'un écran final de profil
 * @param Number redirect - Numéro du profil courante (disponible dans le paramètre de l'URL)
 * @param Object json - Data i18n disponible dans assets/json/*.json
 */
function resultExist({ profile }, { messages: json }) {
    let { en: { screen: { profiles } } } = json
    return profile >= profiles.length || profile <= 0 ? false : true
}
