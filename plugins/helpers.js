import Vue from 'vue';

Vue.prototype.$stripHtml = (html) => {
    let tmp = document.createElement("DIV");
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || "";
}

Vue.prototype.$scrollMeToRef = (ref) => {
    let top = ref.offsetTop - 50
    console.log(top);

    window.scrollTo(0, top)
}
