import Vue from 'vue';

Vue.prototype.$setCookie = (name, value) => {

    let domain = window.location.host.replace(/^(?:https?:\/\/)?(?:www)?/i, "").split('/')[0];

    if (domain.includes(process.env.NUXT_ENV_COOKIE_DOMAIN_NAME)) {
        domain = process.env.NUXT_ENV_COOKIE_DOMAIN_NAME
    }

    let d = new Date();
    d.setTime(d.getTime() + (1000 * 3600 * 24 * 30 * 6));
    let expires = "expires=" + d.toUTCString();
    document.cookie = name + "=" + value + ";" + expires + ";domain=" + domain + ";path=/";
}

Vue.prototype.$getCookie = (_name) => {
    let name = _name + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
