export default ({ app }) => {
    // Règle le problème de la langue par défaut en navigation privé
    app.router.push({path: '/en'})
}
