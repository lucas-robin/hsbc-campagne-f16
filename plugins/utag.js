import Vue from 'vue';

// Config for SPA
window.utag_cfg_ovrd = {
    noview: true
};

// Inject the second script : utag.js
(() => {
    const url = '//tags.tiqcdn.com/utag/hsbc/fr-rbwm/prod/utag.js';
    const elem = document.createElement("script");
    elem.src = url;
    elem.type = 'text/javascript';
    // elem.async = true;
    const firstScriptElement = document.querySelector('script');
    firstScriptElement.parentNode.insertBefore(elem, firstScriptElement);
})();

// Add utag default data to Vue prototype
const utagDefaultViewData = (locale) => {
    return {
        page_type: 'tool',
        page_security_level: '0',
        page_business_line: 'RBWM',
        page_customer_group: 'general',
        site_section: 'PWS',
        tool_name: 'Quiz Expatriés',
        product_category: 'Banque au quotidien',
        product_subcategory: 'Quiz Expatriés',
        page_category: 'Banque au quotidien',
        page_subcategory: 'Quiz Expatriés',
        page_language: locale
    }
}

// Track les "button click"
Vue.prototype.$trackEvent = (event_content) => {

    console.debug('TMS.trackEvent : ' + event_content);

    return TMS.trackEvent({
        event_category: 'content',
        event_action: 'button click',
        event_content
    });

}

// Track les pages
Vue.prototype.$trackView = (params, locale) => {

    console.debug('TMS.trackView');

    return TMS.trackView({
        ...utagDefaultViewData(locale),
        ...params
    });
}


